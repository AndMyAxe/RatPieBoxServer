// Requires
const express = require('express')
//const proxy = require('http-proxy-middleware')
const path = require('path')
const fileServer = require('./FileServer/FileServer.js')
const formidable = require('formidable')
const fs = require('fs')

// For the rat reader
var Epub = require('epub')
var htmlToText = require('html-to-text')

const readerServer = require('./Reader/ServerFunctions.js')

// Startup the chat server part
const chatServer = require('./Chat/Chat.js')

// Initialise the express application
var app = express()

// Tell the application which folder to serve
app.use('/', express.static(path.join(__dirname, 'Pages')))

var tiddlywiki = 'http://pieserver.local:8080'
var gitea = 'http://pieserver.local:3000'
var chat = 'http://pieserver.local/chat.html'
var droppyURL = 'http://pieserver.local:8989'

// Set the port and listen
app.listen(80, function () {console.log('Listening on port 80')})

// the /wiki path redirects to the tiddlywiki server.
//app.use('/wiki', proxy({target: tiddlywiki, changeOrigin: true, ws: true, prependPath: true}))
app.use('/wiki', function (req, res) {
  res.redirect(tiddlywiki)
})

// the gitea path redirects to the gitea server.
app.use('/gitea', function (req, res) {
  res.redirect(gitea)
})

app.use('/chat', function (req, res) {
  res.sendFile(path.join(__dirname, 'Pages', 'chat.html'))
})

app.get('/reader/', function (req, res) {
  res.sendFile(path.join(__dirname, 'Reader', 'index.html'))
})

app.get('/reader/css', function (req, res) {
  res.sendFile(path.join(__dirname, 'Reader', 'reader.css'))
})

app.get('/reader/js', function (req, res) {
  res.sendFile(path.join(__dirname, 'Reader', 'reader.js'))
})

app.get('/reader/settings', function (req, res) {
  res.sendFile(path.join(__dirname, 'Reader', 'Settings', 'Settings.json'))
})

app.get('/reader/settings/:name', function (req, res) {
  name = req.params.name
  if (name) {
    // Send back settings for the selected person
    var personSettings = require(`./Reader/Settings/${name}.json`)
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.end(JSON.stringify(personSettings))
  }
})

app.get('/reader/bookmark', function (req, res) {
  readerServer.bookmark('./Reader/Settings', req, res)
})

app.get('/reader/booklist', function (req, res) {
  readerServer.getBookList('../Files/Books', req, res)
})

app.get('/reader/load/:bookname/:chapter', function (req, res) {
  var bookname = req.params.bookname
  var chapter = req.params.chapter
  if (bookname.endsWith('.epub')) {
    // Load book from the booklist
    var epub = new Epub (`../Files/Books/${decodeURI(bookname)}`)
    //var epub = new Epub (`./Reader/Books/${decodeURI(bookname)}`)
    epub.on("end", function(err){
      res.writeHead(200, {'Content-Type': 'text/plain'})
      epub.getChapter(epub.flow[Number(chapter)].id, function (err, text) {
        res.end(JSON.stringify({text: htmlToText.fromString(text), rawText: text, total: Object.keys(epub.flow).length}))
      })
    })
    epub.parse()
  } else if (bookname.endsWith('.txt')) {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    fs.readFile(`./Reader/Books/${decodeURI(bookname)}`, 'utf8', function(err, data) {
      if (err) {
        console.log(err)
      } else {
        res.end(JSON.stringify({text: data, rawText: null, total: 1}))
      }
    })
  } else {
    res.end()
  }
})

app.get('/reader/loadericon', function (req, res) {
  res.sendFile(path.join(__dirname, 'Reader', 'RatReaderFace.svg'))
})

// This has to be at the end because it is annoying
var http = require('http')
// Set up the part to the file server
// Initialze the droppy server with a config object.
const droppyServer = require('droppy')({
  configdir: "./config",
  filesdir: "/media/PieTin/Files",
  logLevel: 3,
  debug: false
})
var droppyApp = express()
const droppyHTTPServer = http.createServer(droppyApp)
droppyApp.use('/', droppyServer.onRequest)
droppyServer.setupWebSocket(droppyHTTPServer)
droppyHTTPServer.listen(8989)

app.use('/files', function (req, res) {
  res.redirect(droppyURL)
})
