//const url = require('url')
const fs = require('fs')
const path = require('path')
const formidable = require('formidable')

var fileServer = function (req, res) {
  // extract URL path
  let pathname = `.${req.path}`
  let filepath = path.join('/media/PieTin/Files',req.path)
  // based on the URL path, extract the file extention. e.g. .js, .doc, ...
  const ext = path.parse(filepath).ext
  // maps file extention to MIME typere
  const map = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword'
  }

  fs.exists(filepath, function (exist) {
    if(!exist) {
      // if the file is not found, return 404
      res.statusCode = 404
      res.end(`File ${pathname} not found!`)
      return
    }

    // if is a directory search for index file matching the extention
    if (fs.statSync(filepath).isDirectory()) {
      if (fs.exists(`${filepath}/index${ext}`)) {
        filepath += '/index' + ext
      } else {
        // If no index is found list items in the directiory
        fs.readdir(filepath, function (err, items) {
          if (err) {
            res.statusCode = 500
            res.end(`Error: ${err}.`)
          } else {
            var backone = path.join(req.baseUrl,req.path,'..')
            var out = `
            <html>
            <h2>File Explorer</h2>
            Current Location: ${pathname}<br>
            <a href="${backone}">..</a>
            <br/>`
            items.forEach(function(item) {
              var fspath = path.join(filepath,item)
              var serverpath = path.join('/files',req.path,item)
              var serverpath2 = path.join('/filesdl',req.path,item)
              if(fs.statSync(fspath).isDirectory()) {
                out += `<a href="${serverpath}">${item}/</a><br>`
              } else {
                out += `<a href="${serverpath2}">${item}</a> <a href="${serverpath2}" download>(&#8681;)</a><br>`
              }
            })

            out += '<hr><h3>Upload File Here</h3>'
            out += `<form action="/fileupload" method="post" enctype="multipart/form-data">Upload <input type="file" name="filetoupload"> to <input type="text" value="${req.path}" name="path"><br><input type="submit"></form>`
            out += '</html>'

            res.end(out)
          }
        })
      }
    }

    // read file from file system
    var fileLocation = path.join('/media/PieTin/Files', pathname)
    fs.readFile(fileLocation, function(err, data){
      if(err){
        res.statusCode = 500
        res.end(`Error getting the file: ${err}.`)
      } else {
        // if the file is found, set Content-type and send data
        res.setHeader('Content-type', map[ext] || 'text/plain' )
        res.end(data)
      }
    })
  })


}

module.exports = fileServer
