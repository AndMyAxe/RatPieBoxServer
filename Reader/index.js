var http = require('http')
var fs = require('fs')
var path = require('path')

var Epub = require('epub')
var htmlToText = require('html-to-text')

var readerServer = require('./ServerFunctions.js')

// Simple server
http.createServer(function (req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Request-Method', '*')
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET')
  res.setHeader('Access-Control-Allow-Headers', '*')
  if (req.url === '/') {
    res.writeHead(200, {'Content-Type': 'text/html'})
    var content = fs.readFileSync('./index.html')
    res.end(content, 'utf-8')
  } else if (req.url === '/reader/css') {
    res.writeHead(200, {'Content-Type': 'text/css'})
    var content = fs.readFileSync('./reader.css')
    res.end(content, 'utf-8')
  } else if (req.url === '/reader/js') {
    res.writeHead(200, {'Content-Type': 'text/javascript'})
    var content = fs.readFileSync('./reader.js')
    res.end(content, 'utf-8')
  } else if (req.url === '/reader/bookList') {
    // Load the booklist.
    readerServer.getBookList('./Books', req, res)
  } else if (req.url.startsWith('/reader/load/')) {
    var urlparts = req.url.split('/')
    // Check if the book is txt or epub
    if (urlparts[3].endsWith('.epub')) {
      // Load book from the booklist
      var epub = new Epub (`./Books/${decodeURI(urlparts[3])}`)
      epub.on("end", function(err){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        epub.getChapter(epub.flow[Number(urlparts[4])].id, function (err, text) {
          res.end(JSON.stringify({text: htmlToText.fromString(text), rawText: text, total: Object.keys(epub.flow).length}))
        })
      })
      epub.parse()
    } else if (urlparts[3].endsWith('.txt')) {
      res.writeHead(200, {'Content-Type': 'text/plain'})
      fs.readFile(`./Books/${decodeURI(urlparts[3])}`, 'utf8', function(err, data) {
        if (err) {
          console.log(err)
        } else {
          res.end(JSON.stringify({text: data, rawText: undefined, total: 1}))
        }
      })
    }
  } else if (req.url.startsWith('/reader/settings')) {
    var urlparts = req.url.split('/')
    if (!urlparts[3]) {
      // Send back list of people
      var settings = require('./Settings/Settings.json')
      res.writeHead(200, {'Content-Type': 'text/plain'})
      res.end(JSON.stringify(settings))
    } else {
      // Send back settings for the selected person
      var personSettings = require(`./Settings/${urlparts[3]}.json`)
      res.writeHead(200, {'Content-Type': 'text/plain'})
      res.end(JSON.stringify(personSettings))
    }
  } else if (req.url.startsWith('/reader/bookmark')) {
    var urlparts = req.url.slice('10').split("&")
    var query = {
      person: urlparts[0].split("=")[1],
      book: urlparts[1].split("=")[1],
      chapter: urlparts[2].split("=")[1],
      paragraph: urlparts[3].split("=")[1],
      word: urlparts[4].split("=")[1],
      fontsize: urlparts[5].split("=")[1],
      wpm: urlparts[6].split("=")[1],
    }
    readerServer.bookmark('./Settings', req, res, query)
  } else if (req.url === '/reader/loadericon') {
    fs.readFile(path.join(__dirname, 'RatReaderFace.svg'), 'utf8', function(err, data) {
      if (err) {
        console.log(err)
      } else {
        res.writeHead(200, {'Content-Type': 'image/svg+xml'})
        res.end(data)
      }
    })
  }
}).listen(8080, function () {
  console.log('The RatReader is now being served on localhost:8080')
})
