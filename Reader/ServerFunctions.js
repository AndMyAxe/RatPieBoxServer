var fs = require('fs')
var path = require('path')

/*
  Given the directory holding the books this gets a list of books and sends
  them back using the res.
*/
function getBookList(dir, req, res) {
  var booklist = {}
  fs.readdir(dir, function (err, items) {
  //fs.readdir('./Reader/Books', function (err, items) {
    if (err) {
      console.log(err)
    } else {
      // populate booklist with information
      items.forEach(function(item) {
        var extension = path.extname(item)
        if (extension === '.txt' || extension === '.epub') {
          booklist[item] = extension
        }
      })
    }
    res.writeHead(200, {'Content-Type': 'text/plain'})
    var text = JSON.stringify(booklist)
    res.end(text)
  })
}

function bookmark (dir, req, res, query) {
  req.query = req.query || query
  var book = req.query.book
  var person = req.query.person
  //var thisPath = path.join(__dirname, dir, `${person}.json`)
  var thisPath = path.join(__dirname, 'Settings', `${person}.json`)
  var settings = require(thisPath)
  settings = settings || {}
  settings.Bookmarks = settings.Bookmarks || {}
  settings.Bookmarks[book] = settings.Bookmarks[book] || {}
  settings.Bookmarks[book].word = req.query.word || 0
  settings.Bookmarks[book].paragraph = req.query.paragraph || 0
  settings.Bookmarks[book].chapter = req.query.chapter || 0
  settings['font-size'] = req.query.fontsize.slice(0,-2) || 16
  settings.WPM = req.query.wpm || 500
  settings.CurrentBook = req.query.book || ''
  fs.writeFile(thisPath, JSON.stringify(settings, null, 2))
  res.end()
}

module.exports = {}
module.exports.getBookList = getBookList
module.exports.bookmark = bookmark
