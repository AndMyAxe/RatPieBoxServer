var currentParagraph = 0,
  currentIndex = 0,
  centerLetter = '',
  stop = true,
  processID = null,
  previousWord = '',
  nextWord = '',
  text = '',
  words = [],
  paragraphs = [],
  numParagraphs = 0,
  baseDuration = 100,
  local = true,
  currentChapter = 0,
  chapter = '',
  bookName = '',
  loaded = false,
  speaking = false,
  currentPerson = '',
  TotalChapters = 0,
  rawText = '',
  speakingCharacterIndex = 0,
  curXPos = 0,
  curDown = false,
  utteranceStartIndex = 0

var TextContainerElement = document.getElementById('TextContainer')
var TotalParagraphsSpan = document.getElementById('TotalParagraphs')

var synth = window.speechSynthesis
var voices = synth.getVoices()
synth.onvoiceschanged = function () {
  populateVoices()
}
var utterance = new SpeechSynthesisUtterance()
utterance.rate = 1

populateVoices()
addKeyboardShortcuts()
// do this with a callback so that it acutally works correctly
loadPersonList(loadServerList)

setupDragInterface()
addTouchEvents()

/*
  TODO figure out how to make this set up the bulk text once and then only
  update the specific paragraph to show the current word instead of rebuilding
  the whole thing.
*/
function showBulkWords () {
  var paragraphArray = []
  // Hide the serial words by adding the class 'hidden' to the container
  document.getElementById("TextContainer").classList.add('hidden')

  if (rawText) {
    // We have text with markup
    // Get the html by using a fake dom
    var fake = document.createElement('html')
    fake.innerHTML = rawText
    // get paragraphs from html
    paragraphElements = fake.getElementsByTagName('p')
    Array.from(paragraphElements).forEach(function (paragraphElement) {
      paragraphArray.push(paragraphElement.textContent)
    })
  } else {
    // We don't have text with markup, use paragraphs
    paragraphArray = paragraphs
  }

  // Build the bulktext using the paragraphs. Add stuff so we can navigate.
  var bulkText = ""
  paragraphArray.forEach(function(paragraphText, paragraphIndex) {
    var WordSpans = ""
    paragraphText.split(/[\s|\n]+/).forEach(function(word, wordIndex) {
      WordSpans += `<span onclick='setCurrentSpot(${wordIndex},${paragraphIndex})'>${word}</span>` + ' '
    })
    bulkText += `<p class='bulk-paragraph' id="paragraph${paragraphIndex}">
    <span
      class='paragraph-select'
      onclick='setCurrentParagraph(this)'
      index="${paragraphIndex}"
    >
      >
    </span> ${WordSpans}</p>`
  })

  // Put text into the bulk container
  var bulkContainer = document.getElementById("BulkWords")
  bulkContainer.innerHTML = bulkText
  bulkContainer.classList.remove('hidden')
  updateCurrentBulkWord()
  // TODO figure out if we should do the current word instead of the paragraph
  document.getElementById(`paragraph${currentParagraph}`).scrollIntoView()
}

function bulkWordsResetParagraph () {
  var para = document.getElementById(`paragraph${currentParagraph}`)
  if (para) {
    var thisText = para.textContent.slice(8)
    var WordSpans = ""
    thisText.split(/[\s|\n]+/).forEach(function(word, wordIndex) {
      WordSpans += `<span onclick='setCurrentSpot(${wordIndex},${currentParagraph})'>${word}</span>` + ' '
    })
    para.innerHTML =
    `<p class='bulk-paragraph' id="paragraph${currentParagraph}">
      <span
        class='paragraph-select'
        onclick='setCurrentParagraph(this)'
        index="${currentParagraph}"
      >
        >
      </span>
      ${WordSpans}
    </p>`
  }
}

function updateCurrentBulkWord () {
  var paragraphElement = document.getElementById(`paragraph${currentParagraph}`)
  if (paragraphElement) {
    var wordsBefore = words.slice(0, currentIndex).join(' ')
    var wordsAfter = words.slice(currentIndex+1).join(' ')
    document.getElementById(`paragraph${currentParagraph}`).innerHTML =
    `<span
      class='paragraph-select'
      index="${currentParagraph}"
    >
      >
    </span>
    <span>
      ${wordsBefore}
    </span>
    <span style='background-color:grey;border-radius:0.3em;'>${words[currentIndex]}</span>
    <span>
      ${wordsAfter}
    </span>`
    paragraphElement.scrollIntoView()
  }
}

/*
  This is the generic function for setting the current place in the book
*/
function setCurrentSpot (wordIndex, paragraphIndex, chapterIndex) {
  if (chapterIndex && currentChapter !== chapterIndex) {
    // Load the chapter
    var placeInfo = {
      bookName: bookName,
      currentChapter: chapterIndex,
      currentParagraph: paragraphIndex,
      currentIndex: wordIndex
    }
    loadServerText(placeInfo)
  } else {
    if (currentParagraph !== paragraphIndex) {
      // Load the paragraph
      loadParagraph(paragraphIndex)
    }
    // Set the current word
    currentIndex = wordIndex
  }
  showCurrentWord()
}

/*
  This takes a paragraph as a chunk of text and splits it into an array of
  separate words.
*/
function splitParagraphIntoWords (paragraph) {
  // Basic part to split into words on spaces or linebreaks
  paraWords = paragraph.split(/\s|\n/)
  // Remove any zero-length, whitespace, or undefined entries
  paraWords = paraWords.filter(word => typeof word !== 'undefined' && !(/\s/.test(word)) && word !== '')
  // Special case: a word that has - in it
  // This is handled by the splitWord function
  // Special case: a word that has multiple - in it in a row
  // Split this into separate words with the -'s being a word on their own
  var thisSpot = 0
  while (thisSpot < paraWords.length) {
    if (/-{2,}/.test(paraWords[thisSpot])) {
      // First word is everything before the first - is the first word
      var firstWord = ''
      if (paraWords[thisSpot].indexOf('-') > 0) {
        firstWord = paraWords[thisSpot].slice(0,paraWords[thisSpot].indexOf('-'))
      }
      // Between the first and last index of - is the second word
      var secondWord = paraWords[thisSpot].slice(paraWords[thisSpot].indexOf('-'), paraWords[thisSpot].lastIndexOf('-')+1)
      // Everything after the last - is the third word.
      var lastWord = ''
      if (paraWords[thisSpot].lastIndexOf('-')+1 < paraWords[thisSpot].length) {
        lastWord = paraWords[thisSpot].slice(paraWords[thisSpot].lastIndexOf('-')+1)
      }
      // Remove old word
      paraWords.splice(thisSpot, 1)
      if (firstWord && firstWord !== '') {
        paraWords.splice(thisSpot,0,firstWord)
        thisSpot += 1
      }
      if (secondWord && secondWord !== '') {
        paraWords.splice(thisSpot,0,secondWord)
        thisSpot += 1
      }
      if (lastWord && lastWord !== '') {
        paraWords.splice(thisSpot,0,lastWord)
        thisSpot += 1
      }
    } else {
      // Check next word
      thisSpot += 1
    }
  }
  return paraWords
}

function setCurrentParagraph (para, wordIndex) {
  currentParagraph = para.getAttribute('index')
  var CurrentParagraphSpan = document.getElementById('CurrentParagraph')
  CurrentParagraphSpan.innerHTML = currentParagraph + 1
  words = splitParagraphIntoWords(paragraphs[currentParagraph])
  currentIndex = wordIndex || 0
  updateParagraphListing()
  showCurrentWord()
  hideBulkWords()
}

function hideBulkWords () {
  // Show the serial words
  document.getElementById("TextContainer").classList.remove('hidden')

  // Hide the bulk container
  document.getElementById("BulkWords").classList.add('hidden')
}

function showHideBulkWords () {
  if (document.getElementById("TextContainer").classList.contains('hidden')) {
    showBulkWords()
  } else {
    hideBulkWords()
  }
}

function saveBookmark () {
  var fontsize = document.getElementById("TextContainer").style.fontSize
  var wpm = document.getElementById('newWPM').value
  currentPerson = document.getElementById('PersonSelect').value
  var queryString = `?person=${currentPerson}&book=${bookName}&chapter=${currentChapter}&paragraph=${currentParagraph}&word=${currentIndex}&fontsize=${fontsize}&wpm=${wpm}`
  httpRequest = new XMLHttpRequest();
  httpRequest.open('GET', `/reader/bookmark${queryString}`, true);
  httpRequest.send()
}

function addKeyboardShortcuts () {
  document.addEventListener('keydown', function (event) {
    if (event.keyCode === 32) {
      // spacebar
      playPause()
    } else if (event.keyCode === 39) {
      // right
      showNextWord()
    } else if (event.keyCode === 37) {
      // left
      showPreviousWord()
    } else if (event.keyCode === 38) {
      // up
      loadPreviousParagraph('start')
    } else if (event.keyCode === 40) {
      // down
      loadNextParagraph()
    }
  })
}

/*
  This populates the list of voices
*/
function populateVoices () {
  for(i = 0; i < voices.length ; i++) {
    var option = document.createElement('option');
    option.textContent = voices[i].name + ' (' + voices[i].lang + ')';
    if(voices[i].default) {
      option.textContent += ' -- DEFAULT';
    }
    option.setAttribute('data-lang', voices[i].lang);
    option.setAttribute('data-name', voices[i].name);
    document.getElementById("VoiceSelect").appendChild(option);
  }
}

function setUtterance () {
  var voiceSelect = document.getElementById("VoiceSelect")
  var selectedOption = voiceSelect.selectedOptions[0].getAttribute('data-name');
  for(i = 0; i < voices.length ; i++) {
    if(voices[i].name === selectedOption) {
      utterance.voice = voices[i];
    }
  }
}

function loadPersonList(cb) {
  httpRequest = new XMLHttpRequest();
  showLoader()
  httpRequest.open('GET', `/reader/settings`, true);
  httpRequest.onreadystatechange = function(){
    // Process the server response here.
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      // Everything is good, the response was received.
      makePersonSelect(httpRequest.responseText)
      if (typeof cb === 'function') {
        cb()
      }
      hideLoader()
    }
  }
  httpRequest.send()
}

function makePersonSelect (response) {
  var people = JSON.parse(response)
  var options = '<option value="">--</option>'
  people.people.forEach(function (person) {
    options += `<option value='${person}'>${person}</option>`
  })
  var selections = document.getElementById('PersonSelect')
  selections.innerHTML = options
}

function loadPersonSettings () {
  currentPerson = document.getElementById('PersonSelect').value
  httpRequest = new XMLHttpRequest();
  showLoader()
  httpRequest.open('GET', `/reader/settings/${currentPerson}`, true);
  httpRequest.onreadystatechange = function(){
    // Process the server response here.
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      // Everything is good, the response was received.
      updateSettings(JSON.parse(httpRequest.responseText))
      hideLoader()
    }
  }
  httpRequest.send();
}

function updateSettings (settings) {
  placeInfo = {
    bookName: '',
    currentChapter: 0,
    currentIndex: 0,
    currentParagraph: 0
  }
  if (settings['font-size']) {
    document.getElementById('newSize').value = settings['font-size']
    document.getElementById("TextContainer").style.fontSize = settings['font-size'] + 'px';
  }
  if (settings['background-color']) {
    document.getElementsByTagName('body')[0].style.backgroundColor = settings['background-color']
  }
  if (settings['font-family']) {
    document.getElementById('TextContainer').style.fontFamily = settings['font-family']
  }
  if (settings.WPM) {
    document.getElementById('newWPM').value = settings.WPM
    // Convert wpm to milliseconds per word
    baseDuration = Number(1000/settings.WPM*60)
  }
  if (settings.border) {
    document.getElementById('TextContainer').style.border = settings.border
  }
  if (settings.centerLetter) {
    if (settings.centerLetter.color) {
      document.getElementById('Center').style.color = settings.centerLetter.color
    }
  }
  if (settings.otherWords) {
    if (settings.otherWords.color) {
      document.getElementById('PreviousWord').style.color = settings.otherWords.color
      document.getElementById('NextWord').style.color = settings.otherWords.color
    }
  }
  if (settings.currentWord) {
    if (settings.currentWord.color) {
      document.getElementById('FirstHalf').style.color = settings.currentWord.color
      document.getElementById('LastHalf').style.color = settings.currentWord.color
    }
  }
  if (settings.CurrentBook) {
    if (settings.Bookmarks) {
      if (settings.Bookmarks[settings.CurrentBook]) {
        placeInfo = {
          bookName: settings.CurrentBook,
          currentChapter: Number(settings.Bookmarks[settings.CurrentBook].chapter) || 0,
          currentIndex: Number(settings.Bookmarks[settings.CurrentBook].word) || 0,
          currentParagraph: Number(settings.Bookmarks[settings.CurrentBook].paragraph) || 0
        }
      }
    }
    loadServerText(placeInfo)
  }
  showCurrentWord()
}

/*
  Load list of books on the server
*/
function loadServerList () {
  httpRequest = new XMLHttpRequest();
  httpRequest.open('GET', `/reader/bookList`, true);
  showLoader()
  httpRequest.onreadystatechange = function(){
    // Process the server response here.
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      // Everything is good, the response was received.
      makeBookSelect(httpRequest.responseText)
      hideLoader()
    }
  }
  httpRequest.send();
}

function makeBookSelect (response) {
  var books = JSON.parse(response)
  var options = ''
  Object.keys(books).forEach(function (book) {
    options += `<option value='${book}'>${book}</option>`
  })
  var selections = document.getElementById('BookSelect')
  selections.innerHTML = options
}

/*
  Load the main bookmark for this book
*/
function loadBookmark () {
  var settings = {}
  var book = bookName || document.getElementById('BookSelect').value
  var httpRequest = new XMLHttpRequest()
  showLoader()
  httpRequest.open('GET', `/reader/settings/${currentPerson}`, true)
  httpRequest.onreadystatechange = function () {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      settings = JSON.parse(httpRequest.responseText)
      if (settings.Bookmarks[book]) {
        placeInfo = {
          currentIndex: settings.Bookmarks[book].word,
          currentParagraph: settings.Bookmarks[book].paragraph,
          currentChapter: settings.Bookmarks[book].chapter,
          bookName: book
        }
        loadServerText(placeInfo)
        hideLoader()
      }
    }
  }
  httpRequest.send()
}

/*
  Load text from the server.
*/
function loadServerText (placeInfo) {
  local = false
  placeInfo = placeInfo || {}
  bookName = placeInfo.bookName || document.getElementById('BookSelect').value
  currentChapter = Number(placeInfo.currentChapter) || 0
  currentParagraph = Number(placeInfo.currentParagraph) || 0
  currentIndex = Number(placeInfo.currentIndex) || 0
  document.getElementById('CurrentBook').innerHTML = bookName
  if (bookName) {
    httpRequest = new XMLHttpRequest();
    showLoader()
    httpRequest.open('GET', `/reader/load/${bookName}/${currentChapter}`, true);
    httpRequest.onreadystatechange = function(){
      // Process the server response here.
      if (httpRequest.readyState === XMLHttpRequest.DONE) {
        // Everything is good, the response was received.
        rawText = JSON.parse(httpRequest.responseText).rawText
        preparedText = JSON.parse(httpRequest.responseText).text
        paragraphs = preparedText.split(/(?:\r?\n){2,}/)
        words = splitParagraphIntoWords(paragraphs[currentParagraph])
        TotalChapters = JSON.parse(httpRequest.responseText).total
        updateChapterDisplay()
        updateParagraphListing()
        showCurrentWord()
        loaded = true
        hideLoader()
      }
    }
    hideModal()
    httpRequest.send()
  }
}

function updateChapterDisplay () {
  document.getElementById('TotalChapters').innerHTML = TotalChapters
  document.getElementById('CurrentChapter').innerHTML = Number(currentChapter) + 1
}

/*
  Load text from a local file.
*/
function loadText () {
  local = true
  var reader = new FileReader()
  var fileInput = document.getElementById('book_text')
  var file = fileInput.files[0]
  bookName = file.name
  reader.onload = function (e) {
    preparedText = reader.result
    // We need both pieces of the regex here because windows is weird
    paragraphs = preparedText.split(/(?:\r?\n){2,}/)
    updateParagraphListing()
    words = splitParagraphIntoWords(paragraphs[currentParagraph])
    loaded = true
    showCurrentWord()
  }
  hideModal()
  reader.readAsText(file)
}

function updateParagraphListing () {
  TotalParagraphsSpan.innerHTML = paragraphs.length
  document.getElementById('CurrentParagraph').innerHTML = Number(currentParagraph) + 1
}

/*
  This updates the delay between words based on the input words per minute
*/
function updateDelay () {
  var thing = document.getElementById('newWPM')
  // Convert wpm to milliseconds per word
  baseDuration = Number(1000/thing.value*60)
}

/*
  This function updates the words displayed in the reader section of the screen
*/
function updateDisplay (parts, previousWord, nextWord) {
  var PreviousContainer = document.getElementById('PreviousWord')
  var NextContainer = document.getElementById('NextWord')
  var FirstHalfContainer = document.getElementById('FirstHalf')
  var CenterContainer = document.getElementById('Center')
  var LastHalfContainer = document.getElementById('LastHalf')
  PreviousContainer.innerHTML = previousWord
  NextContainer.innerHTML = nextWord
  FirstHalfContainer.innerHTML = parts.firstBit
  CenterContainer.innerHTML = parts.centerLetter
  LastHalfContainer.innerHTML = parts.lastBit
}

/*
  This function handles changes to the font size
*/
function updateFontSize () {
  var thing = document.getElementById('newSize')
  document.getElementById("TextContainer").style.fontSize = thing.value + 'px';
}

function showCurrentWord () {
  var previousWord = currentIndex > 0 && words[currentIndex-1]? words[currentIndex-1]:''
  var nextWord = currentIndex < words.length && words[currentIndex+1]? words[currentIndex+1]:''
  var parts = splitWord(words[currentIndex])
  updateDisplay(parts, previousWord, nextWord)
  updateCurrentBulkWord()
  return parts.delay
}

/*
  This function shows the next word by preparing the word and then updating the
  dispaly as needed.
*/
function showNextWord () {
  if (loaded) {
    if (currentIndex < words.length-1) {
      currentIndex += 1
      var delay = showCurrentWord()
      if (!stop) {
        if (!speaking) {
          clearTimeout(processID)
          processID = setTimeout(showNextWord, delay)
        }
      }
    } else {
      loadNextParagraph()
      if (currentParagraph < paragraphs.length) {
        if (!stop) {
          if (!speaking) {
            clearTimeout(processID)
            var delay = 5*baseDuration > 500 ? 500 : 5*baseDuration
            processID = setTimeout(showNextWord, delay)
          }
        }
      }
    }
  }
}

/*
  This displays the next word when the text is being spoken.
  Switching to the next paragraph is handled by the speechEnd function given to
  the utterance as the onend event handler. Otherwise the last word gets cut
  off.
*/
function speakNextWord () {
  if (loaded) {
    if (currentIndex < words.length-1) {
      showCurrentWord()
      currentIndex += 1
    } else {
      showCurrentWord()
    }
  }
}

/*
  This moves the current text position back by one word.
*/
function showPreviousWord() {
  if (loaded) {
    if (currentIndex > 0) {
      currentIndex -= 1
      showCurrentWord()
    } else {
      previousParagraph()
    }
  }
}

/*
  This moves the current text position forward by one paragraph.
*/
function nextParagraph () {
  if (currentParagraph < numParagraphs && loaded) {
    stop = true
    loadNextParagraph()
  } else {
    loadNextParagraph()
  }
}

/*
  This moves the current text position back by one paragraph.
*/
function previousParagraph () {
  if (currentParagraph > 0 && loaded) {
    stop = true
    loadPreviousParagraph()
  } else {
    loadPreviousChapter()
  }
}

/*
  This loads the previous paragraph
*/
function loadPreviousParagraph (position) {
  var position = position || 'end'
  if (loaded) {
    if (currentParagraph > 0) {
      loadParagraph(currentParagraph-1, position)
    } else if (currentChapter > 0) {
      loadPreviousChapter()
    }
  }
}

/*
  This handles what to do when the speech synthesis thing is finished.
  This generally means load the next paragraph.
  This fires only when the speech is finished on safari, but in firefox this
  fires when you pause or cancel it also. So checking the current index is
  necessary.
*/
function speechEnd () {
  if (currentIndex === words.length - 1) {
    loadNextParagraph()
    currentIndex = 0
    showCurrentWord()
  }
}

/*
  This is how everything keeps track of where the speech synthesis is, keeping
  everything in sync.
*/
function boundaryListener (event) {
    // This tries to pull off the first word of the text from where the speech
    // is currently
    var currentWordThing = /[^\s|\n]+/.exec(words.slice(utteranceStartIndex).join(' ').slice(event.charIndex))
    // make sure that the currentWordThing is an actual word listed in the
    // words array. Non-word whitespace and junk gets caught by the speech
    // synthesis thing so we need this.
    if (words.indexOf(currentWordThing[0]) !== -1) {
      speakingCharacterIndex = event.charIndex
      currentIndex = words.indexOf(currentWordThing[0], currentIndex)
      speakNextWord()
    }
}

/*
  This loads a paragraph by index
*/
function loadParagraph (paraIndex, position) {
  position = position ? position:'start'
  if (loaded) {
    if (paraIndex < paragraphs.length) {
      bulkWordsResetParagraph()
      var CurrentParagraphSpan = document.getElementById('CurrentParagraph')
      currentParagraph = paraIndex
      CurrentParagraphSpan.innerHTML = currentParagraph + 1
      words = splitParagraphIntoWords(paragraphs[currentParagraph])
      if (speaking) {
        currentIndex = 0
        if (synth.pending) {
          synth.cancel()
        }
        utterance = new SpeechSynthesisUtterance()
        setUtterance()
        var currentText = words.slice(currentIndex).join(' ')
        utteranceStartIndex = currentIndex
        utterance.text = currentText
        utterance.onboundary = boundaryListener
        utterance.onend = speechEnd
        if (!stop) {
          synth.speak(utterance)
        }
      } else {
        currentIndex = position==='start'?-1:words.length-2
        showNextWord()
      }
      updateCurrentBulkWord()
    }
  }
}

/*
  This loads the next paragaph
*/
function loadNextParagraph () {
  if (currentParagraph+1 < paragraphs.length) {
    loadParagraph(currentParagraph+1, 'start')
    if (speaking) {
      if (!stop) {
        synth.speak(utterance)
      }
    }
  } else if (local === false) {
    loadNextChapter()
  }
}

/*
  Loads the next chapter from the server.
*/
function loadNextChapter (paragraphIndex, wordIndex) {
  if (!stop) {
    stop = true
    var tempStop = true
  }
  if (bookName) {
    if (currentChapter < document.getElementById('TotalChapters').innerHTML-1 || (paragraphIndex && wordIndex)) {
      currentChapter += 1
      httpRequest = new XMLHttpRequest();
      showLoader()
      httpRequest.open('GET', `/reader/load/${bookName}/${currentChapter}`, true);
      httpRequest.onreadystatechange = function(){
        // Process the server response here.
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
          // Everything is good, the response was received.
          TotalChapters = JSON.parse(httpRequest.responseText).total
          rawText = JSON.parse(httpRequest.responseText).rawText
          preparedText = JSON.parse(httpRequest.responseText).text
          paragraphs = preparedText.split(/(?:\r?\n){2,}/)
          currentParagraph = Number(paragraphIndex) || 0
          currentIndex = Number(wordIndex) || 0
          words = splitParagraphIntoWords(paragraphs[currentParagraph])
          updateChapterDisplay()
          updateParagraphListing()
          if (tempStop) {
            stop = false
          }
          showCurrentWord()
          loaded = true
          local = false
          if (!stop) {
            if (speaking) {
              speakNextWord()
            } else {
              var delay = 5*baseDuration > 500 ? 500 : 5*baseDuration
              processID = setTimeout(showNextWord, delay)
            }
          }
          hideLoader()
        }
      }
      httpRequest.send();
    }
  }
}

function loadPreviousChapter () {
  if (bookName) {
    if (currentChapter > 0) {
      currentChapter -= 1
      httpRequest = new XMLHttpRequest();
      showLoader()
      httpRequest.open('GET', `/reader/load/${bookName}/${currentChapter}`, true);
      httpRequest.onreadystatechange = function(){
        // Process the server response here.
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
          // Everything is good, the response was received.
          paragraphs = JSON.parse(httpRequest.responseText).text.split(/(?:\r?\n){2,}/)
          numParagraphs = Number(paragraphs.length)
          TotalParagraphsSpan.innerHTML = numParagraphs
          currentParagraph = 0
          currentIndex = 0
          document.getElementById('CurrentParagraph').innerHTML = Number(currentParagraph) + 1
          words = splitParagraphIntoWords(paragraphs[currentParagraph])
          updateChapterDisplay()
          updateParagraphListing()
          showCurrentWord()
          hideLoader()
        }
      }
      httpRequest.send();
    }
  }
}

/*
  This prepares the current word for display.
*/
function splitWord (word) {
  if (word) {
    var first, last, center, delay, wordLength
    wordLength = word.length

    if (wordLength > 2) {
      timeOut = wordLength > 10 ? 2*baseDuration : baseDuration+0.1*baseDuration*wordLength
    } else {
      timeOut = 1.5*baseDuration
    }

    timeOut = wordLength > 2 ? baseDuration+0.1*baseDuration*wordLength : 1.5*baseDuration

    if (wordLength > 1) {
      // if any of the things in this array are in a word than the reader stays
      // on that word for some extra time.
      var delayArray = ['\\?', '\\:', '\\,', '\\.', '\\;']
      var delayThing = new RegExp(delayArray.join('|')).test(word)

      timeOut = delayThing ? 1.25*timeOut : timeOut

      // If a word has a dash in it than the dash is the center.
      // This seems to work well.
      var dashIndex = word.indexOf('-')
      if (dashIndex > 0) {
        parts = word.split('-')
        first = parts[0]
        last = parts[1]
        center = '-'
      } else {
        var len1 = Math.floor(word.length/2)
        var len2 = word.length - len1
        first = len1 > 0 ? word.substring(0,len1-1):''
        last = len2 > 0 ? word.substring(len1):''
        if (len2 === 0) {
          center = word.substring(-1)
        } else {
          center = word.substring(len1-1,len1)
        }
      }
    } else {
      first = ''
      last = ''
      center = word
    }
    return {firstBit: first, lastBit: last, centerLetter: center, delay: timeOut}
  } else {
    return {firstBit: "", lastBit: "", centerLetter: "", delay: 10}
  }
}

/*
  If the reader is playing than stop it, otherwise start it.
*/
function playPause() {
  if (loaded) {
    if (speaking) {
      if (stop) {
        if (synth.pending) {
          synth.cancel()
        }
        utterance = new SpeechSynthesisUtterance()
        setUtterance()
        stop = false
        var currentText = words.slice(currentIndex).join(' ')
        utteranceStartIndex = currentIndex
        utterance.text = currentText
        utterance.onboundary = boundaryListener
        utterance.onend = speechEnd
        synth.speak(utterance)
      } else {
        stop = true
        utterance.removeEventListener('boundary', boundaryListener)
        utterance.removeEventListener('end', speechEnd)
        synth.cancel()
      }
    } else {
      clearTimeout(processID)
      if (synth) {
        synth.cancel()
      }
      if (stop) {
        stop = false
        showNextWord()
      } else {
        stop = true
      }
    }
  }
}

/*
  Toggle speech
*/
function toggleSpeech () {
  var speechButton = document.getElementById('ToggleSpeech')
  if (speaking) {
    speaking = false
    speechButton.value='Speech: OFF'
  } else {
    speaking = true
    speechButton.value='Speech: ON'
  }
}

/*
  Display the modal for loading a book
*/
function showLoadBookModal () {
  document.getElementById("LoadBookModal").style.display="block"
}

/*
  Display the modal for the voice settings
*/
function showVoiceModal () {
  document.getElementById("VoiceSettingsModal").style.display="block"
}

/*
  Hide all the modals
*/
function hideModal () {
  document.getElementById("LoadBookModal").style.display="none"
  document.getElementById("VoiceSettingsModal").style.display="none"
}

/*
  This toggles the visibility of the UI bar that has the person select and
  other configuration options.
*/
function showHideBar () {
  document.getElementById("IconBar").classList.toggle('icon-bar')
  document.getElementById("IconBar").classList.toggle('icon-bar-hidden')

  document.getElementById("ShowSettingsButton").classList.toggle('show-settings-icon')
  document.getElementById("ShowSettingsButton").classList.toggle('show-settings-icon-rotate')
}

/*
  This lets you click and drag to move froward and backward through the text
*/
function setupDragInterface () {
  var textContainer = document.getElementById("TextContainer")
  var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.getElementByName("body").clientWidth
  textContainer.addEventListener('mousemove', function (e) {
    if (curDown) {
      if (e.pageX > windowWidth/10 + curXPos) {
        // Previous word
        showPreviousWord()
        curXPos = e.pageX
      } else if (e.pageX < curXPos - windowWidth/10) {
        // Next word
        showNextWord()
        curXPos = e.pageX
      }
    }
  })
  textContainer.addEventListener('mousedown', function (e) {
    curXPos = e.pageX
    curDown = true
  })
  textContainer.addEventListener('mouseup', function (e) {
    curDown = false
  })
  textContainer.addEventListener('mouseleave', function (e) {
    curDown = false
  })
}

/*
  This allows touch interaces to also use the text drag thing
*/
function touchHandler (event) {
  var touches = event.changedTouches,
    first = touches[0],
    type = ""
  switch(event.type) {
    case "touchstart": type = "mousedown"; break;
    case "touchmove": type = "mousemove"; break;
    case "touchend": type = "mouseup";   break;
    default: return;
  }

  var simulatedEvent = document.createEvent("MouseEvent");
  simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);

  first.target.dispatchEvent(simulatedEvent)
}

/*
  Add the handlers for the touch events
*/
function addTouchEvents () {
  document.addEventListener("touchstart", touchHandler, true);
  document.addEventListener("touchmove", touchHandler, true);
  document.addEventListener("touchend", touchHandler, true);
  document.addEventListener("touchcancel", touchHandler, true);
}

/*
  This shows the loading indictator
*/
function showLoader () {
  document.getElementById("LoaderDiv").classList.remove('hidden')
}

/*
  This hides the loading indicator
*/
function hideLoader () {
  document.getElementById("LoaderDiv").classList.add('hidden')
}
