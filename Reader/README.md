# Rat Reader

This is a in-browser RSVP reader application. Support for text files and EPUB files is in place and other formats are planned.

Currently only text files can be opened locally and only epubs can be opened from the server. This should be fixed soon.

## What does it look like?

If you want to see what the reader looks like here is a screen shot:

![A screen shot of the UI](./UIScreenShot.png?raw=true "UI Screen Shot")

# How to use this

1. Clone the repo
2. Open `index.html` in a browser
3. Click the `Choose File` button, select a `.txt` file on your computer
4. Click the `Load Text` button
5. Press `Play` to start, `Stop` to stop (or pause)

To run the server start the server with `node index.js` in the RatReader folder. If you have the server running you can put `.epub` files in the `Books` folder, then in the browser click `Load Book List` which will populate a list of books from the folder. Select the book you want and then click `Load Text`. Navigation and configuration is the same as for local files.

There can be multiple profiles. You can select a name from the `Select Person`
menu and then click `Load Settings` to load the settings for that profile.
If you click the `Bookmark` button it saves the current spot in the book and
other settings.
Once you have bookmarked a spot in a book for a profile clicking
`Load Settings` will load the book to that spot as well as loading the
configuration.

The `<` and `>` buttons move froward or backward by one word, the `<<` and `>>` buttons move forward or backward by one paragraph, the `<<<` and `>>>` buttons skip forward or backward one chapter in epubs. You may have to click
the buttons twice in order to get it to move the first time, after that it should be one click per word/paragraph. You can set the font size (in px units) and words per minute. Changes don't take effect until you click the `Update` button next to the option you change.

Keyboard shortcuts:
- Space - Start/stop
- right arrow - forward one word
- left arrow - back one word
- up arrow - forward one paragraph
- down arrow - backward one paragraph

# Current features

- Load and parse local .txt files (UTF-8 encoding, windows or unix line
  endings)
- Load and parse .epub or .txt files from a server (I don't know if I can parse epubs in the browser)
- Set font size
- Set words per minute to be displayed
- Skip ahead/back by one word
  - clicking on the next/previous word
  - left and right arrow keys
  - the `<` and `>` buttons
- Skip ahead/back by one paragraph
  - The up and down arrow keys
  - the `<<` and `>>` buttons
- Skip ahead/back by one chapter
  - the `<<<` and `>>>` buttons
- Start/Stop the reader
  - Spacebar
  - clicking on the current word
  - Clicking the play/pause button
- Keyboard shortcuts
- Per person Settings
  - A persons settings and last saved reading place are loaded when you select a person from the selection menu
  - WPM and font size are saved when you make a bookmark
  - Unfortunately you have to manually edit a json file for styling
- Per person bookmarks
  - At the moment only one per book, you set it by clicking the `Bookmark` button
- Word delay based on word length and punctuation
  - This can probably use tuning
- Click and drag lets you move the words forward and backward
  - swiping also works on touch screens
  - The drag/touch has to stay inside the text area, moving outside the text
  area will stop the drag event
- A loading indicator for when a new chapter or book is loading from the server

## Working but imperfect features

- Text-to-speech capability
  - The text-to-speech can start at the current spot reading, and you can start reading at the current text-to-speech part (they are synced).
  - Timing needs some work
  - Pick voices and language from the list of what is available
- Read normally (bulk text laid out)
  - Touch a word to set your new place
  - TTS should work in both modes (highlight the current word to keep the spot)
  - Clicking on a word should set it as the current word (probably, test this)

# Roadmap

- Add options to change the color scheme
- find a better way to access the configuration menu
- UI to change settings
- Add table of contents navigation for things with TOCs
- Add better bookmarking, like an editable list of persistent bookmarks
  -  A UI to have multiple bookmarks and select which one to go to.
- Options to set custom delay values
  - caused by punctuation and paragraph breaks.
  - also chapters and paragraphs
- Disable buttons that aren't useable (like chapters buttons when there is only one chapter).
  - Do this by color indicators, don't remove the buttons or the layout will change.
