const WebSocketServer = require('ws').Server

// This is the port used by the web socket server
const SERVER_PORT = 8082
// Create the web socket server on the defined port
var wss = new WebSocketServer({port: SERVER_PORT})

// Set the onconnection function
wss.on('connection', handleConnection)

var history = []
var clients = []

var nodeMessageHandlers = {}

const colors = ['red','green','blue','magenta','purple','plum','orange']

function handleConnection(client) {
  console.log("new connection")
  clients.push({'socket':client, 'active': true})
  client.on('message', function incoming(event) {
    var self = this
    // Determine which connection the message came from
    try {
      var eventData = JSON.parse(event)
      // Add the source to the eventData object so it can be used later.
      // Make sure we have a handler for the message type
      if (typeof nodeMessageHandlers[eventData.type] === 'function') {
        nodeMessageHandlers[eventData.type](eventData)
      } else {
        console.log('No handler for message of type ', eventData.type)
      }
    } catch (e) {
      console.log("WebSocket error, probably closed connection: ", e)
    }
  })
}

/*
{
  name: Person sending the message
  message: the message
  time: a timestamp
}
*/
nodeMessageHandlers.chat = function (eventData) {
  // Add message to chat history
  history.push(
    {
      text: eventData.message,
      name: eventData.name,
      time: eventData.timestamp
    }
  )
  // Push the history to all clients
  updateClients()
}

/*
  Send the history to each client
*/
var updateClients = function () {
  console.log('update clients')
  var message = JSON.stringify({type: 'history', history: history})
  clients.forEach(function (client) {
    console.log(`a client ${message}`)
    client.socket.send(message)
  })
}
